#!/usr/bin/env bash

# ${DESCRIPTION}
#
# Copyright (c) ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} (${COPYRIGHT_CONTACT})
# SPDX-License-Identifier: ${LICENSE_SPDX_ID}



# Important Stuff
#

set -o nounset  # Disallow unset variables and parameters.
set -o noglob   # Disable pathname expansion
#set -o errexit  # Exit on non-zero command status (errors).  TODO: Potentially troublesome!
set -o pipefail # Pipelines return value is exit value of last failing command





# Constants
#

readonly myname="$(basename "${BASH_SOURCE[0]}")"  # $BASH_SOURCE for compatibility w/ bats testing.
readonly version="${RELEASE_VERSION}"

readonly usage_short="$myname [-h] [-v] [-V] [-n] [-d TEMPDIR] [-D] [-e EDITOR]"
readonly usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -d    Path to temp file directory
    -D    Do not delete file on exit
    -e    Editor command"

readonly exit_success=0
readonly exit_error=1

readonly return_success=0
readonly return_error=1





# Defaults
#

declare -g editor="${EDITOR:-}"
declare -g temp_dir="${TMPDIR:-/tmp}"
declare -gi delete_file_on_exit=1

# Make command line options global for sake of bats testing.
declare -gi verbose=0
declare -gi dry_run=0





# Subroutines
#


echo_verbose() {
    # Echo output to stderr if verbose option is enabled.
    #
    # GLOBALS:
    #   - verbose
    #
    # ARGS: $@ passed on unmolested
    #
    # OUTPUT: possible output to stderr
    #
    # RETURN VAL: implicit (echo)

    if [[ $verbose -eq 1 ]] ; then
        echo "$@" >&2
    fi
}



echo_func_err() {
    # Echo function error message to stderr.
    #
    # GLOBALS:
    #   - FUNCNAME
    #
    # ARGS: $@ passed on unmolested
    #
    # OUTPUT: output to stderr
    #
    # RETURN VAL: implicit (echo)

    echo "${FUNCNAME[1]}: $@" >&2
}



_parse_cli_opts() {
    # Process command line options.

    local -r cli_opts="hvVnd:De:"
    local opt OPTIND OPTARG OPTERR

    while getopts ":${cli_opts}" opt; do
        case $opt in
            h)
                echo "$usage_long"  # Need quotes to preserve newlines!
                exit $exit_success
                ;;
            v)
                echo "$myname $version"
                exit $exit_success
                ;;
            V)
                verbose=1
                echo "Verbose mode requested." >&2
                ;;
            n)
                dry_run=1
                echo "Dry run mode requested." >&2
                ;;
            d)
                temp_dir="$OPTARG"
                echo_verbose "Temp file directory: $temp_dir"
                ;;
            D)
                delete_file_on_exit=0
                echo_verbose "Will not delete file upon exit."
                ;;
            e)
                editor="$OPTARG"
                ;;
            :)
                echo "Dude, where's my arg? ($OPTARG)"
                echo "$usage_short"
                exit $exit_error
                ;;
            \?)
                echo "Invalid option: $OPTARG"
                echo "$usage_short"
                exit $exit_error
                ;;
        esac
    done

    return $((OPTIND-1))  # Number of options to discard.
}



main() {
    _parse_cli_opts "$@"  ;  shift $?

    # Check for unconsumed args.
    if [[ $@ ]]; then
        echo "Unconsumed arguments: '$@'"
        echo "$usage_short"
        return $return_error
    fi

    # Determine text editor.
    if [ -z "$editor" ]; then
        echo "Must set \$EDITOR environment variable or specify via \`-e\` command line option." >&2
        echo "$usage_long"
        return $return_error
    else
        command -v "$editor" >/dev/null
        if [[ $? -ne 0 ]] ; then
            echo "Can't find command: $editor" >&2
            return $return_error
        fi
    fi

    # Create temp file.
    temp_file=$(mktemp --tmpdir="$temp_dir" --suffix .txt)
    chmod 600 $temp_file

    # Optionally delete temp file on exit.
    if ((delete_file_on_exit)) ; then
        trap "rm -f $temp_file" INT HUP QUIT KILL EXIT
        echo_verbose "$temp_file"
    else
        echo "$temp_file"
    fi

    # Open temp file in editor (maybe).
    if ((dry_run)) ; then
        echo $editor $temp_file
    else
        eval $editor $temp_file
    fi
}





if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
    if [[ $? -ne $return_success ]] ; then
        exit $exit_error
    else
        exit $exit_success
    fi
fi
