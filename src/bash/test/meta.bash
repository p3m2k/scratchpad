export swut="scratchpad"
dependencies="mktemp"

expected_version="1.0.0"

expected_status_success=0
expected_status_error=1


expected_usage_short="$swut [-h] [-v] [-V] [-n] [-d TEMPDIR] [-D] [-e EDITOR]"
expected_usage_long="$expected_usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -d    Path to temp file directory
    -D    Do not delete file on exit
    -e    Editor command"

expected_output_dry_run_opt="Dry run mode requested."

assets_d="${BATS_TEST_DIRNAME}/assets"
