load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash



# Test Support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!
    # Global vars declared in sourced SWUT can be overridden in test cases with
    # `local VARNAME=VAL` declarations.

    assert_dependencies $dependencies
}


fake_cmd() {
    local -r first="$1"
    local -r second="$2"

    echo "First: \"$first\""
    echo "Second: \"$second\""

    if [[ "$first" == "one" ]]  &&  [[ "$second" == "one" ]] ; then
        echo "Snake eyes!  Failure so sad."
        return 1
    else
        echo "Success!"
        return 0
    fi
} ; export -f fake_cmd



# Common Tests
#

@test "echo_verbose()" {
    local output="This is only a test."

    source_swut "$swut"
    local -r verbose=1
    run echo_verbose "$output"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$output" ]]  # FIXME: confirm stderr
}


@test "echo_func_err()" {
    local err_msg="This is only a test."

    source_swut "$swut"
    run echo_func_err "$err_msg"

    assert_status $expected_status_success
    assert_lines_qty 1
    # NB: The calling function name depends on bats internals!
    [[ "${lines[0]}" == "bats_merge_stdout_and_stderr: $err_msg" ]]  # FIXME: confirm stderr
}


@test "_parse_cli_opts()" {
    # Is this even necessary?  We'd mostly be testing `getopts`.
    skip
}





# vim: syntax=bash tw=0
