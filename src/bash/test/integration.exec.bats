load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash



# Test support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!

    assert_dependencies $dependencies

    #export stdopts="-n"
    export prog_temp_dir="/tmp"
    export re_temp_file='tmp\.[a-zA-Z0-9]{10}\.txt'
    export EDITOR="bestedit"
}



# Common options
#

@test "no opts, no args" {
    mock -d "$BATS_TEST_TMPDIR" $EDITOR
    run exec_swut

    assert_status $expected_status_success
    assert_lines_qty 0
    assert_file_grep_string "${prog_temp_dir}/${re_temp_file}" "${MOCK_BESTEDIT_CALLS_DIR}/1.txt"
}


@test "invalid opt" {
    run exec_swut -z

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Invalid option: z" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "missing opt arg" {
    run exec_swut -e

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Dude, where's my arg? (e)" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "help" {
    run exec_swut -h

    assert_status $expected_status_success
    assert_lines_qty 9  # bats ignores blank lines.
    [[ "$output" == "$expected_usage_long" ]]
}


@test "version" {
    run exec_swut -v

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$swut $expected_version" ]]
}


@test "verbose option" {
    mock -d "$BATS_TEST_TMPDIR" $EDITOR
    run exec_swut -V

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "Verbose mode requested." ]]
    [[ "${lines[1]}" =~ ${prog_temp_dir}/${re_temp_file} ]]
    assert_file_grep_string "${prog_temp_dir}/${re_temp_file}" "${MOCK_BESTEDIT_CALLS_DIR}/1.txt"
}


@test "dry run option" {
    mock -d "$BATS_TEST_TMPDIR" $EDITOR
    run exec_swut -n

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" =~ $EDITOR\ ${prog_temp_dir}/${re_temp_file} ]]
}



# Script-specific
#

# NB: Configuration via EDITOR and TMPDIR environment variables is tested
# implicitly in the following cases:


@test "alt temp dir option" {
    mock -d "$BATS_TEST_TMPDIR" $EDITOR
    run exec_swut -V -d $BATS_TEST_TMPDIR

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "Verbose mode requested." ]]
    [[ "${lines[1]}" == "Temp file directory: $BATS_TEST_TMPDIR" ]]
    [[ "${lines[2]}" =~ $BATS_TEST_TMPDIR/${re_temp_file} ]]
    assert_file_grep_string "$BATS_TEST_TMPDIR/${re_temp_file}" "${MOCK_BESTEDIT_CALLS_DIR}/1.txt"
}


@test "no-delete option" {
    mock -d "$BATS_TEST_TMPDIR" $EDITOR
    export TMPDIR="$BATS_TEST_TMPDIR"  # Why must this be exported?
    run exec_swut -V -D

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "Verbose mode requested." ]]
    [[ "${lines[1]}" == "Will not delete file upon exit." ]]
    [[ "${lines[2]}" =~ ${BATS_TEST_TMPDIR}/${re_temp_file} ]]
    assert_file_grep_string "$BATS_TEST_TMPDIR/${re_temp_file}" "${MOCK_BESTEDIT_CALLS_DIR}/1.txt"
    assert_file_found "${lines[2]}"
}


@test "alt editor command option" {
    local editor="worstedit"
    mock -d "$BATS_TEST_TMPDIR" "$editor"
    run exec_swut -V -e "$editor"

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "Verbose mode requested." ]]
    [[ "${lines[1]}" =~ ${prog_temp_dir}/${re_temp_file} ]]
    assert_file_grep_string "${prog_temp_dir}/${re_temp_file}" "${MOCK_WORSTEDIT_CALLS_DIR}/1.txt"
}


@test "missing editor command" {
    local editor="worstedit"
    run exec_swut -e "$editor"

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "Can't find command: worstedit" ]]
}


@test "no editor specified" {
    unset EDITOR
    run exec_swut

    assert_status $expected_status_error
    assert_lines_qty 10
    [[ "${lines[0]}" == "Must set \$EDITOR environment variable or specify via \`-e\` command line option." ]]
    [[ "$output" =~ "$expected_usage_long" ]]
}





# vim: syntax=bash tw=0
