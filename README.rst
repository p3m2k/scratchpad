##########
Scratchpad
##########


Introduction
============

*Scratchpad* is a simple, command-line program that opens a temporary
file in your favorite text editor.  And that’s all!



Installation
============

*Scratchpad* is a Bash_ shell script.  Almost every Linux system
includes Bash.  Users of other Unix-like_ systems may need to install it
before using this program.

Scratchpad has no special requirements.

There are two easy ways to install Scratchpad:

- Packages_ are available for users of Debian and its derivatives
  (Ubuntu, etc.).  Users of Red Hat, Fedora and friends: stay tuned!

- Download an archive of the latest release and run ``make install``:

  .. code:: console

     # wget -O - https://www.nellump.net/downloads/scratchpad_latest.tgz | tar xz
     # cd scratchpad/
     # make install

bats_ is required to run the automated test suite (of interest only to
programmers).

.. _Bash: https://www.gnu.org/software/bash/
.. _Unix-like: http://www.linfo.org/unix-like.html
.. _packages : https://www.nellump.net/computers/free_software
.. _bats: https://github.com/bats-core/bats-core



Usage
=====

See the `manpage <src/man/scratchpad.1.raw.rst>`_.



Future Plans
============

None at this time.



Copying
=======

Everything here is copyright © 2017-2022 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
