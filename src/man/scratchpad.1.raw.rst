#######
${SLUG}
#######

${DESCRIPTION}
==============

:author: ${AUTHOR_NAME}
:contact: ${AUTHOR_CONTACT}
:copyright: © ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} <${COPYRIGHT_CONTACT}>
:manual_section: 1
:version: ${RELEASE_VERSION}
:date: ${RELEASE_DATE}



Synopsis
--------

${SLUG} [-h] [-v] [-V] [-n] [-d TEMPDIR] [-D] [-e EDITOR]



Options
-------

-h
    Print usage
-v
    Print version
-V
    Verbose mode
-n
    Dry run mode
-d
    Path to temp file directory (defaults to ``/tmp``)
-D
    Do *not* delete temp file on exit
-e
    Editor command (name or full path)



Environment
-----------

EDITOR
    Editor command (name or full path).  Specification via command line
    option (``-e``) takes precedence.



Exit Status
-----------

0
    Success
1
    Failure



See Also
--------

``mktemp``\ (1)
